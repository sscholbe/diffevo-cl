// Schaffer Function N. 4
__kernel void eval(
    __constant double *restrict pop,
    __global double *restrict costs,
    unsigned num_pop,
    unsigned num_attr,
    __constant double *restrict eval_data,
    __local double *restrict local_data
) {
    const unsigned id = get_global_id(0);
    const unsigned t = id * num_attr;
    
    const double x = pop[t], y = pop[t + 1];
    costs[id] = 0.5 + (pow(cos(sin(fabs(x * x - y * y))), 2) - 0.5)
        / (pow(1 + 0.001 * (x * x + y * y), 2));
}
