#include <stdlib.h>
#include <stdio.h>
#include "../diffevo/diffevo.h"

int main(void) {
    diffevo_params_t params = {0};
    params.num_pop = 50;
    params.num_attr = 2;
    params.num_iter = 600;
    params.shrink = 0.6;
    params.crossover = 0.5;
    params.mu = 0;
    params.sigma = 10;
    
    double best[2], cost = 0;

    if (0 != diffevo_solve("eval_schaffer.cl", &params, best, &cost)) {
        return 1;
    }

    printf("minimum: x: %f y: %f with cost %f\n", best[0], best[1], cost);

    getchar();

    return 0;
}
